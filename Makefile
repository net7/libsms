CXX = /usr/bin/clang++
CXXFLAGS = -std=c++17 -Iinclude/ -fPIC -Wall -Wextra -Werror -W -O2 -g
LDFLAGS = -Iinclude/ -shared -lboost_system -lcurl
TARGET_LIB = libsms.so

SRCS = src/http_parser.cc src/server.cc src/sms.cc
SRCS_TEST = ${SRCS} tests/simple_echo.cc
OBJS = $(SRCS:.cc=.o)

all: ${TARGET_LIB}

$(TARGET_LIB): $(OBJS)
	$(CXX) ${LDFLAGS} -o $@ $^

$(SRCS:.cc=.d):%.d:%.cc
	$(CXX) -MM $(CXXFLAGS) $< > $@

include $(SRCS:.cc=.d)

test: all
	$(CXX)  -o simple_echo_port_9000 tests/simple_echo.cc -lsms -lboost_system

install: all
	mv ${TARGET_LIB} /usr/lib/ && cp -r include/sms/ /usr/include/

clean:
	-rm -rf ${TARGET_LIB} ${OBJS} $(SRCS:.cc=.d)

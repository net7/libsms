# libsms
###### Librairie C++ pour la réception d'SMS via HTTP et _SMS Gateway_

## Présentation
`libsms` est une librairie C++ qui offre un support simple pour gérer un
 serveur HTTP minimaliste qui réceptionne les SMS d'un téléphone via
 l'application [SMS Gateway](https://play.google.com/store/apps/details?id=eu.apksoft.android.smsgateway&hl=fr).
 Dans le futur, elle pourra gérer n'importe quel type de _SMS Gateway_.

## Pré-requis
`libsms` a besoin de la librairie [Boost](http://www.boost.org/) et d'un
compilateur pour compiler et fonctionner.

#### Installation de boost

Sur **Debian**:
```bash
sudo apt install libboost-all-dev g++
```

Sur ArchLinux:
```bash
pacman -S boost g++
```


## Installation

```bash
git clone 'https://git.inpt.fr/baillet/libsms.git'
cd libsms
make && sudo make install
```

#### Compiler les tests
```bash
make test
```

## Usage et documentation

Premièrement, on inclut la librairie :
```c++
#include <sms/server.hpp>
#include <sms/sms.hpp>
```
`sms/server.hpp` contient la partie concernant le serveur HTTP.
`sms/sms.hpp` contient le modèle SMS.

## class `sms::Server`

##### Description
Serveur HTTP gérant l'arrivée des SMS

##### Constructeurs
```cpp
Server::Server(std::string& host, unsigned short port)
```
###### Paramètres
 - `std::string& host`: adresse d'écoute
 - `unsigned short port`: port d'écoute
###### Exceptions
**TODO**

##### Méthodes

```cpp
bool set_event_handler(std::unique_ptr<Handler> handler)
```
###### Description
Initialise le gestionnaire d'événement au serveur.

###### Paramètres
 - `std::unique_ptr<handler>`: le gestionnaire d'événement (voir `Handler` plus
 loin)
###### Valeur de retour
`true` si le gestionnaire a été correctement initialisé, `false` sinon.

--
```cpp
void start_accept()
```
###### Description
Signaler que l'on est prêt à accepter des clients à _asio::io_service_.

###### Paramètres
aucun

###### Valeur de retour
aucune

--
```cpp
void run()
```
###### Description
Démarrer le serveur

###### Paramètres
aucun

###### Valeur de retour
aucune


## class `sms::Handler`

##### Description
Gestionnaire d'événément pour le serveur

##### Constructeurs
Aucun

##### Méthodes
Aucune

##### Attributs

```cpp
Event m_event
```
Type d'événement, parmi :
```cpp
enum Event {
  MESSAGE
};
```

--

```cpp
std::function<void(std::unique_ptr<Sms> sms)> m_callback
```
Fonction de retour lors d'un événément.
`sms` est nul pour tous les événéments autres que `MESSAGE`.


## class `sms::Sms`
###### `#include sms/sms.hpp`

##### Description
Un SMS.

##### Constructeurs
```cpp
Sms::Sms(std::string& number, std::string& content, SmsType type)
```
###### Paramètres
 - `std::string& number`: Numéro de téléphone du destinateur
 - `std::string& content`: Contenu du SMS
 - `SmsType type`: Type de SMS (SMS ou MMS)

##### Méthodes

```cpp
const std::string& get_content()
```
###### Description
Récupérer le contenu du SMS

###### Paramètres
aucun

###### Valeur de retour
Le contenu du SMS

--

```cpp
const std::string& get_number()
```
###### Description
Récupérer le numéro du destinataire

###### Paramètres
aucun

###### Valeur de retour
Le numéro du destinataire

--

```cpp
SmsType get_type()
```
###### Description
Récupérer le type de SMS.

###### Paramètres
aucun

###### Valeur de retour
Le type de SMS (SMS ou MMS).
**ATTENTION**: n'est supporté aujourd'hui que les SMS.

## Exemples

Simple serveur, écoutant sur le port 9000, qui écrit dans _stdout_ le
numéro de téléphone et le contenu d'un sms :

```cpp
#include <sms/server.hpp>
#include <sms/sms.hpp>
#include <iostream>


/* Fonction qui sera appelée par le serveur lors de la réception d'un
 * sms */
void message_handler(std::unique_ptr<Sms> sms) {
  std::cout << "On a reçu " << '<' << sms->get_number() << "> " << sms->get_content() << std::endl;
}

int main() {
  std::string host = "127.0.0.1";
  sms::Server server(host, 9000);

  sms::Handler handler;
  handler.m_event = sms::Event::MESSAGE;
  handler.m_callback = message_handler;

  server.set_event_handler(std::make_unique<Handler>(handler);
  server.start_accept();
  server.run();

}
```

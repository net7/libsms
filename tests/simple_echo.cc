/*
             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.



 Author: zadig
*/
#include <sms/server.hpp>
#include <sms/sms.hpp>
#include <iostream>

/* Fonction qui sera appelée par le serveur lors de la réception d'un sms */
void message_handler(std::unique_ptr<sms::Sms> sms) {
  std::cout << "On a reçu " << '<' << sms->get_number() << "> " << sms->get_content() << std::endl;
}

int main() {
  std::string host = "127.0.0.1";
  sms::Server server(host, 9000);

  sms::Handler handler;
  handler.m_event = sms::Event::MESSAGE;
  handler.m_callback = message_handler;

  server.set_event_handler(std::make_unique<sms::Handler>(handler));
  server.start_accept();
  server.run();

}
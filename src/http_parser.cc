/*
             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.



 Author: zadig
*/

#include <sms/http_parser.hpp>
#include <iostream>

namespace sms {


int HttpParser::parse() {
  std::string buffer(m_buffer);
  buffer.erase(std::remove(buffer.begin(), buffer.end(), '\r'), buffer.end());
  std::istringstream stream(buffer);
  std::string s;
  int ret = NO_ERROR;
  if (std::getline(stream, s, '\n')) {
    ret = parse_first_line(s);
  } else {
    ret = BAD_REQUEST;
  }
  if (!ret) {
    while(std::getline(stream, s, '\n') && !s.empty() && s.find_first_not_of(' ') != std::string::npos) {
      ret = parse_header(s);
    }
  }
  return ret;
}

int HttpParser::parse_first_line(std::string &line) {
  int ret = NO_ERROR;
  std::istringstream stream(line);
  std::string method;
  if (std::getline(stream, method, ' ')) {
    m_method = std::string(method);
    auto slash_separator = line.find_first_of('/');
    if (slash_separator != std::string::npos) {
      auto end = line.find_first_of(' ', slash_separator);
      if (end != std::string::npos) {
        m_path = std::string(line.substr(slash_separator, (end - slash_separator)));
      } else {
        ret = BAD_REQUEST;
      }
    } else {
      ret = BAD_REQUEST;
    }
  } else {
    ret = BAD_REQUEST;
  }

  return ret;
}

int HttpParser::parse_header(std::string &line) {
  int ret = NO_ERROR;
  auto separator_pos = line.find_first_of(':');
  if (separator_pos == std::string::npos) {
    ret = BAD_REQUEST;
  } else {
    Header header;
    header.m_name = boost::algorithm::trim_copy(line.substr(0, separator_pos));
    header.m_value = boost::algorithm::trim_copy(line.substr(separator_pos + 1, std::string::npos));
    uri_decode(header.m_value);
    uri_decode(header.m_name);
    m_headers.push_back(header);
  }

  return ret;
}

std::vector<Header>& HttpParser::get_headers() {
  return m_headers;
}

void HttpParser::uri_decode(std::string& data) {
  int out_length;
  size_t s;
  while ((s = data.find_first_of('+')) && s != data.npos) {
    data.replace(s, 1, " ");
  }
  char *decoded = curl_easy_unescape(m_curl_easy_handle, data.c_str(), data.size(), &out_length);
  data = std::string(decoded, out_length);
  curl_free(decoded);
}

HttpParser::~HttpParser() {
  curl_easy_cleanup(m_curl_easy_handle);

}

} /* namespace sms */

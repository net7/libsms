/*
             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.



 Author: zadig
*/

#include <iostream>
#include <sms/server.hpp>


namespace sms {

Server::Server(std::string &host, unsigned short port) {
  m_host = host;
  m_port = port;
  m_acceptor = std::make_unique<boost::asio::ip::tcp::acceptor>(
          m_io_service,
          boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), m_port));
}

bool Server::set_event_handler(std::unique_ptr<Handler> handler) {
  bool added = false;
  switch (handler->m_event) {
    case MESSAGE:
      m_sms_handler = std::move(handler);
      added = true;
      break;
    default:
      break;
  }

  return added;
}

boost::asio::io_service &Server::get_io_service() {
  return m_io_service;
}

void Server::start_accept() {
  auto connection = std::make_shared<Connection>(*this);
  m_acceptor->async_accept(connection->get_socket(),
               std::bind(&Server::handle_accept, this, connection,
                           std::placeholders::_1));
}

void Server::handle_accept(std::shared_ptr<Connection> connection, const boost::system::error_code &error) {
  if (!error) {
    m_connections.push_back(connection);
    connection->run();
  }
  this->start_accept();
}

void Server::remove_connection(Connection* connection) {
  size_t i = 0;
  while (i < m_connections.size() && m_connections.at(i)->m_uuid != connection->m_uuid && i++);
  if (i != m_connections.size()) {
    m_connections.erase(m_connections.begin() + i);
  }
}

void Server::run() {
  if (m_sms_handler != nullptr) {
    m_io_service.run();
  } else {
    throw ServerConfigurationException("No SMS handler was specified.");
  }
}

void Server::notify_on_sms_received(std::unique_ptr<Sms> sms) {
  m_sms_handler->m_callback(std::move(sms));
}

boost::asio::ip::tcp::socket &Connection::get_socket() {
  return m_socket;
}



void Connection::run() {
  m_socket.async_read_some(
          boost::asio::buffer(m_data, MAX_HTTP_CONTENT_LENGTH),
          std::bind(&Connection::receive_http_request, this, std::placeholders::_1, std::placeholders::_2)
    );
}

void Connection::receive_http_request(const boost::system::error_code &error, size_t bytes_transferred) {
  if (!error) {
    parse_http_request(bytes_transferred);
    std::string response("HTTP/1.1 200 OK\nContent-Length: 0\n\n");
    m_socket.send(boost::asio::buffer(response));
  } else {
    std::cout << "Error: " << error << std::endl;

  }
  m_socket.close();
  m_server.remove_connection(this);
}

void Connection::parse_http_request(size_t length) {
  HttpParser parser(m_data, length);
  if (!parser.parse()) {
    std::string phone;
    std::string text;
    for(Header& h: parser.get_headers()) {
      if (h.m_name == "phone") {
        phone = std::string(h.m_value);
      } else if (h.m_name == "text") {
        text = std::move(h.m_value);
      }
    }
    if (!phone.empty() && !text.empty()) {
      std::unique_ptr<Sms> sms_to_send;
      sms_to_send = std::make_unique<Sms>(phone, text, SmsType::SMS);
      m_server.notify_on_sms_received(std::move(sms_to_send));
    }
  }
}

const char *ServerConfigurationException::what() {
  return m_errstr;
}


}

/** @headerfile */
/*
             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.



 Author: zadig
*/

#ifndef SMS_MANAGER_HTTP_PARSER_HPP
#define SMS_MANAGER_HTTP_PARSER_HPP

#include <sstream>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <curl/curl.h>

#define NO_ERROR 0x0
#define BAD_METHOD -0x1
#define BAD_REQUEST -0x2

/**
 * @brief main libsms namespace.
 */
namespace sms {

/**
 * @brief An HTTP header.
 */
class Header {
  public:
    /**
     * Name of the HTTP header
     */
    std::string m_name;

    /**
     * Value of the HTTP header
     */
    std::string m_value;
};

/**
 * @brief Parser for HTTP requests
 */
class HttpParser {
  public:
    /**
     * @brief Construct a HTTP parser.
     *
     * @param buffer - Buffer which contains the HTTP request
     * @param buffer_length - Length of the buffer
     */
    HttpParser(char *buffer, size_t buffer_length): m_buffer(buffer),
                                                    m_buffer_length(buffer_length),
                                                    m_curl_easy_handle(curl_easy_init()){};

    /**
     * @brief Parse the buffer which contains the HTTP request.
     *
     * @return An error code
     */
    int parse();

    /**
     * @brief Get a list of the header from the HTTP request.
     *
     * @return Vector of Headers
     */
    std::vector<Header>& get_headers();

    ~HttpParser();

  private:

    std::string m_method;
    std::string m_path;
    std::vector<Header> m_headers;
    char *m_buffer;
    [[maybe_unused]] size_t m_buffer_length;
    CURL* m_curl_easy_handle;

    int parse_first_line(std::string& line);
    int parse_header(std::string& line);
    void uri_decode(std::string& data);
};

} /* namespace sms */


#endif //SMS_MANAGER_HTTP_PARSER_HPP

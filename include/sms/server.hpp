/** @headerfile */
/*
             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.



 Author: zadig
*/


#ifndef SMS_MANAGER_SMS_SERVER_HPP
#define SMS_MANAGER_SMS_SERVER_HPP

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <vector>

#include <sms/http_parser.hpp>
#include <sms/sms.hpp>

#define MAX_HTTP_CONTENT_LENGTH 4096

namespace sms {

/**
 * @brief Events for the HTTP server.
 */
enum Event {
  /**
   * A SMS arrived
   */
  MESSAGE
};

/**
 * @brief Exception thrown when user provided a bad configuration for the server
 */
class ServerConfigurationException {

  public:
    /**
     * @brief Construct an ServerConfigurationException with the string-representation of the error.
     *
     * @param errstr String-representation of the error
     */
    ServerConfigurationException(const char *errstr): m_errstr(errstr) {};

    /**
     * @brief Get the string-representation of the error.
     *
     * @return string-representation of the error
     */
    const char* what();

  private:
    /**
     * String-representation of the error
     */
    const char *m_errstr;
};


/**
 * @brief An Handler for a certain type of event
 */
class Handler {
  public:
    /**
     * Event which is handle by the handler.
     */
    Event m_event;

    /**
     * Callback which will be called when the event is fired.
     */
    std::function<void(std::unique_ptr<Sms> sms)> m_callback;
};

class Connection;

/**
 * @brief A HTTP-Server for receiving the SMS.
 */
class Server {
    friend class Connection;

  public:
    /**
     * @brief Construct a HTTP-Server for receiving the SMS.
     *
     * @param host - Host for binding the server
     * @param port - Port for the server
     */
    Server(std::string& host, unsigned short port);

    /**
     * @brief Get the ASIO context instance.
     *
     * @return ASIO context instance
     */
    boost::asio::io_service& get_io_service();

    /**
     * @brief Assign an Handler for a specific event.
     *
     * @param handler - Handler to be assigned
     * @return true if the handler has been assigned successfully, otherwise else
     */
    bool set_event_handler(std::unique_ptr<Handler> handler);

    /**
     * @brief Prepare the server for receiving incoming connections.
     */
    void start_accept();

    /**
     * @brief Launch the server.
     */
    void run();

  private:
    boost::asio::io_service m_io_service;
    std::unique_ptr<boost::asio::ip::tcp::acceptor> m_acceptor;
    unsigned short m_port;
    std::string m_host;
    std::unique_ptr<Handler> m_sms_handler;
    std::vector<std::shared_ptr<Connection>> m_connections;
    void handle_accept(std::shared_ptr<Connection> connection,
                       const boost::system::error_code& error);
    void remove_connection(Connection* connection);
    void notify_on_sms_received(std::unique_ptr<Sms> sms);

};

/**
 * @brief A connection to the HTTP-server
 */
class Connection: std::enable_shared_from_this<Connection> {
    friend class Server;

  public:
    /**
     * @brief Construct a connection.
     *
     * @param server - Server which handle this connection
     */
    explicit Connection(Server& server): m_socket(server.get_io_service()),
                                         m_server(server),
                                         m_data{0},
                                         m_uuid(boost::uuids::random_generator()()) {}
  private:
    boost::asio::ip::tcp::socket m_socket;
    Server& m_server;
    char m_data[MAX_HTTP_CONTENT_LENGTH];
    boost::uuids::uuid m_uuid;

    void run();
    boost::asio::ip::tcp::socket& get_socket();
    void receive_http_request(const boost::system::error_code& error, size_t bytes_transferred);
    void parse_http_request(size_t length);

};

} /* namespace sms */

#endif //SMS_MANAGER_SMS_SERVER_HPP

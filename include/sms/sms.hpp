/*
             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.



 Author: zadig
*/

#ifndef SMS_MANAGER_SMS_HPP
#define SMS_MANAGER_SMS_HPP

#include <string>

namespace sms {

enum SmsType {
  SMS,
  MMS
};

class Sms {
  private:
    std::string m_number;
    std::string m_content;
    SmsType m_type;
  public:
    Sms(std::string& number, std::string& content, SmsType type): m_number(number), m_content(content), m_type(type) {};
    const std::string& get_content();
    const std::string& get_number();
    SmsType get_type();
};

} /* namespace sms */

#endif //SMS_MANAGER_SMS_HPP
